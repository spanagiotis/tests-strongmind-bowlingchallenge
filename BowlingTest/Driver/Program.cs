﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace BowlingTest
{
    /// <summary>
    /// Note about scores ending in strikes or spares.
    /// If there are no additional shots that match the scoring criteria (1 shot for spares, 2 shots for strikes), then the score displayed will be the score UP-TO those shots.
    /// We cannot obviously score strikes or spares without their scoring criteria. From my research (looking at bowling scorecards, woo) the score ends at the last calculatable frame - which I mimick as well.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Hello User.");
                Console.WriteLine("Welcome to the bowling score calculator! Here are some simple instructions on how to enter your shots!");
                Console.WriteLine("Each frame entered should be seperated by a '|' and each shot in a frame should be seperated by a ','.");
                Console.WriteLine("");
                Console.WriteLine("Here is an example with 4 frames: 4,5|10|7,0|9,1");
                Console.WriteLine("");
                Console.WriteLine("The number of frames entered may not exceed 10. There are a maximum of 2 shots per frame, except the 10th frame which allows 3 shots max");

                do
                {
                    Console.WriteLine("");
                    Console.WriteLine("To exit the application enter the letter X (not case sensitive)");
                    Console.WriteLine("");
                    Console.Write("Enter Your Shots Here:> ");

                    string input = Console.ReadLine();
                    if (input.ToLower() == "x") { break; }

                    ErroneousInput validationResult = ValidateInput(input);
                    if (validationResult == null)
                    {
                        Scoreboard scoreBoard = GenerateScoreboard(input);
                        scoreBoard.CalculateScore();
                        int score = scoreBoard.GetScore();
                        Console.Clear();
                        Console.WriteLine($"Input:> {input}");
                        Console.WriteLine($"Your Score:> {score}");
                    }
                    else
                    {
                        Console.WriteLine("");
                        Console.WriteLine("----------------------------------------------");
                        Console.WriteLine("");
                        Console.WriteLine("Whoops. Looks like your input was invalid");
                        Console.WriteLine($"Input:> {input}");
                        Console.WriteLine("");
                        Console.WriteLine($"Reason:> {validationResult.Message}");
                    }
                }
                while (true);
            }
            catch (Exception ex)
            {
                Console.WriteLine("");
                Console.WriteLine("Whoa there! Looks like we've run into an issue. Clearly we didn't do something right on our end, or missed some sanitation checks. Dang.");
                Console.WriteLine("Press any key to exit as we can't recover!");
                Console.WriteLine($"{ex.Message}\n{ex.StackTrace}");
                Console.ReadLine();
            }
        }

        /// <summary>
        /// This will run through the string and test it for various edge cases. If I had some more time (it was getting late) I could have extracted each of these "ErroneousInput" class objects into invidual validators with discreet logic, kept a
        /// list of them and looped the input string through the list and returning on the first non-null validator. This was the hardest part of the entire program - making sure to validate the input!
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        static ErroneousInput ValidateInput(string input)
        {
            input.Trim();
            if (string.IsNullOrWhiteSpace(input)) { return new ErroneousInput() { Message = "Your input was null, empty, or contains nothing but whitespace...why?" }; }
            if (Regex.IsMatch(input, "[^0-9,|]")) { return new ErroneousInput() { Message = "Your input contains invalid characters. Only characters allowed: Numbers, pipe (|), comma (,)." }; }
            if (input[input.Length - 1] == '|') { return new ErroneousInput() { Message = "Your input ends with a pipe. Did you forget a frame perhaps?" }; }
            if (input[input.Length - 1] == ',') { return new ErroneousInput() { Message = "Your input ends with a comma. May want to get to fixing that." }; }

            char FRAME_SEPERATOR = '|';
            char SHOT_SEPERATOR = ',';
            string[] frameInputs = input.Split(FRAME_SEPERATOR);

            if (frameInputs.Length > Helper.MAXIMUM_FRAME_COUNT) { return new ErroneousInput() { Message = "Your frame length exceeds 10." }; }
            if (frameInputs.Length == 0) { return new ErroneousInput() { Message = "Your frame length is 0." }; }

            for (int frameInputIndex = 0; frameInputIndex < frameInputs.Length; frameInputIndex++)
            {
                string[] shotInputs = frameInputs[frameInputIndex].Split(SHOT_SEPERATOR);
                int[] shots = new int[shotInputs.Length];
                for (int shotInputIndex = 0; shotInputIndex < shotInputs.Length; shotInputIndex++)
                {
                    if (string.IsNullOrWhiteSpace(shotInputs[shotInputIndex])) { return new ErroneousInput() { Message = $"A frame contains an erroeneous shot." }; }
                    shots[shotInputIndex] = System.Convert.ToInt32(shotInputs[shotInputIndex]);
                    if (shots[shotInputIndex] > Helper.STRIKE_PIN_COUNT) { return new ErroneousInput() { Message = $"A frame contains a total pin count greater than 10...hmm.." }; }
                }

                if (frameInputIndex == Helper.MAXIMUM_FRAME_COUNT - 1)
                {
                    if (shotInputs.Length == 0 || shotInputs.Length == 1) { return new ErroneousInput() { Message = $"Your final frame contains {shotInputs.Length} shot(s)." }; }
                    if (shots[0] == Helper.STRIKE_PIN_COUNT && shots.Length != 3) { return new ErroneousInput() { Message = $"Your final frame should contain 3 shots." }; }
                    if (shots[0] == Helper.STRIKE_PIN_COUNT && shots[1] == Helper.STRIKE_PIN_COUNT && shots.Length != 3) { return new ErroneousInput() { Message = $"Your final frame should contain 3 shots." }; }
                    if (shots[0] + shots[1] == Helper.STRIKE_PIN_COUNT && shots.Length != 3) { return new ErroneousInput() { Message = $"Your final frame should contain only 3 shots." }; }
                    if (shots[0] + shots[1] != Helper.STRIKE_PIN_COUNT && shots[0] != Helper.STRIKE_PIN_COUNT && shots[2] != Helper.STRIKE_PIN_COUNT && shots.Length == 3) { return new ErroneousInput() { Message = $"Your final frame should contain only 2 shots." }; }
                    if (shotInputs.Length > 3) { return new ErroneousInput() { Message = "Your final frame contains more than 3 shots." }; }
                }
                else
                {
                    if (shots.Sum() > Helper.STRIKE_PIN_COUNT) { return new ErroneousInput() { Message = $"A frame contains a total pin count greater than 10...hmm.." }; }
                    if (shotInputs.Length == 0) { return new ErroneousInput() { Message = "A frame contains no shots." }; }
                    if (shots[0] == Helper.STRIKE_PIN_COUNT && shots.Length != 1) { return new ErroneousInput() { Message = $"A frame with a strike contains more than 1 shot." }; }
                    if (shots.Length > 1 && shots[1] == Helper.STRIKE_PIN_COUNT) { return new ErroneousInput() { Message = $"A frame has a strike not in the first shot." }; }
                    if (shotInputs.Length > 2) { return new ErroneousInput() { Message = "A contains more than 2 shots." }; }
                }
            }

            return null;
        }

        /// <summary>
        /// There is a bit of duplicated behavior here from the validation method, namely the parsing of the strings. If I had more time I would have created a more robust validation system that should
        /// have returned a "ValidInput" object with the data already properly parsed into this method. At the end of the day, the software needs to work per specifications.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        static Scoreboard GenerateScoreboard(string input)
        {
            char FRAME_SEPERATOR = '|';
            char SHOT_SEPERATOR = ',';
            string[] frameInputs = input.Split(FRAME_SEPERATOR);

            List<IBowlingFrame> frames = new List<IBowlingFrame>();
            for (int frameInputIndex = 0; frameInputIndex < frameInputs.Length; frameInputIndex++)
            {
                string[] shotInputs = frameInputs[frameInputIndex].Split(SHOT_SEPERATOR);
                int[] shots = new int[shotInputs.Length];
                for (int shotInputIndex = 0; shotInputIndex < shotInputs.Length; shotInputIndex++)
                {
                    shots[shotInputIndex] = System.Convert.ToInt32(shotInputs[shotInputIndex]);
                }

                BowlingFrame bowlingFrame = new BowlingFrame(frameInputIndex, shots);
                frames.Add(bowlingFrame);
            }

            return new Scoreboard(frames);
        }
    }
}