﻿using System.Collections.Generic;

namespace BowlingTest
{
    internal static class Helper
    {
        public static int FIRST_BOWLING_FRAME = 0;
        public static int FINAL_BOWLING_FRAME = 10 - 1;
        public static int STANDARD_FRAME_SHOT_COUNT = 2;
        public static int STRIKE_PIN_COUNT = 10;
        public static int MAXIMUM_FRAME_COUNT = 10;

        public static IBowlingFrame GetBowlingFrame(List<IBowlingFrame> list, int index)
        {
            if (index < 0) { return null; }
            if (index > list.Count - 1) { return null; }
            return list[index];
        }
    }
}
