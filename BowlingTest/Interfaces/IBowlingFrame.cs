﻿using System.Collections.Generic;

namespace BowlingTest
{
    internal interface IBowlingFrame
    {
        int Score { get; }
        int FrameIndex { get; }
        IReadOnlyList<int> Shots { get; }
        void CalculateScore(List<IBowlingFrame> bowlingFrames);
    }
}
