﻿using System.Collections.Generic;

namespace BowlingTest
{
    internal interface IScoreCalculator
    {
        int Calculate(int currentFrame, IReadOnlyList<int> shots, List<IBowlingFrame> bowlingFrames);
    }
}
