﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BowlingTest
{

    internal class BowlingFrame : IBowlingFrame
    {
        public int Score { get; private set; }
        public int FrameIndex { get; private set; }
        public IReadOnlyList<int> Shots { get; private set; }
        private IScoreCalculator _scoreCalculator = null;

        public BowlingFrame(int frameIndex, int[] shots)
        {
            FrameIndex = frameIndex;
            Shots = new ReadOnlyCollection<int>(shots);
            DetermineScoreCalculatorToUse();
        }

        public void CalculateScore(List<IBowlingFrame> bowlingFrames)
        {
            Score = _scoreCalculator.Calculate(FrameIndex, Shots, bowlingFrames);
        }

        private void DetermineScoreCalculatorToUse()
        {
            if (Shots.Count == 1 && Shots[0] == Helper.STRIKE_PIN_COUNT)
            {
                _scoreCalculator = new StrikeScoreCalculator();
            }
            else if (Shots.Count == Helper.STANDARD_FRAME_SHOT_COUNT && Shots[0] + Shots[1] == Helper.STRIKE_PIN_COUNT)
            {
                _scoreCalculator = new SpareScoreCalculator();
            }
            else
            {
                _scoreCalculator = new StandardScoreCalculator();
            }
        }
    }
}