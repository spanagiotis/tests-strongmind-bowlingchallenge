﻿using System.Collections.Generic;
using System.Linq;

namespace BowlingTest
{
    /// <summary>
    /// You'll notice something interesting about the scoreboard class. The calculate function does not return and to get the score we extract it from the last frame.
    /// So why a non-return CalculateScore and a seperate GetScore()? SRP. CalculateScore() should just process the score internally and GetScore() should be the function that gives the current score.
    /// </summary>
    internal class Scoreboard
    {
        public List<IBowlingFrame> Frames { get; private set; }

        public Scoreboard(List<IBowlingFrame> frames)
        {
            Frames = frames;
        }

        public void CalculateScore()
        {
            for (int frameIndex = 0; frameIndex < Frames.Count; frameIndex++)
            {
                IBowlingFrame frame = Frames[frameIndex];
                frame.CalculateScore(Frames);
            }
        }

        public int GetScore()
        {
            return Frames.Last().Score;
        }
    }
}
