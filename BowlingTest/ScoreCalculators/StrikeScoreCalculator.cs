﻿using System.Collections.Generic;
using System.Linq;

namespace BowlingTest
{
    internal class StrikeScoreCalculator : IScoreCalculator
    {
        public int Calculate(int frame, IReadOnlyList<int> shots, List<IBowlingFrame> bowlingFrames)
        {
            int previousShotScore = 0;
            if (frame != Helper.FIRST_BOWLING_FRAME) { previousShotScore = bowlingFrames[frame - 1].Score; }
            if (frame == Helper.FINAL_BOWLING_FRAME)
            {
                return previousShotScore + shots.Sum();
            }
            else
            {
                IBowlingFrame nextFrame = Helper.GetBowlingFrame(bowlingFrames, frame + 1);
                if (nextFrame == null)
                {
                    return previousShotScore;
                }
                else
                {
                    if (nextFrame.Shots.Count >= Helper.STANDARD_FRAME_SHOT_COUNT)
                    {
                        return previousShotScore + shots.Sum() + nextFrame.Shots[0] + nextFrame.Shots[1];
                    }
                    else
                    {
                        int firstBonusShot = nextFrame.Shots[0];
                        IBowlingFrame nextNextFrame = Helper.GetBowlingFrame(bowlingFrames, frame + 2);
                        if (nextNextFrame == null)
                        {
                            return previousShotScore;
                        }
                        else
                        {
                            int secondBonusShot = nextNextFrame.Shots[0];
                            return previousShotScore + shots.Sum() + firstBonusShot + secondBonusShot;
                        }
                    }
                }
            }
        }
    }
}
