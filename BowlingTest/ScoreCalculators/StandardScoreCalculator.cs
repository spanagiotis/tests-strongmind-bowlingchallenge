﻿using System.Collections.Generic;
using System.Linq;

namespace BowlingTest
{
    internal class StandardScoreCalculator : IScoreCalculator
    {
        public int Calculate(int frame, IReadOnlyList<int> shots, List<IBowlingFrame> bowlingFrames)
        {
            int previousShotScore = 0;
            if (frame != Helper.FIRST_BOWLING_FRAME) { previousShotScore = Helper.GetBowlingFrame(bowlingFrames, frame - 1).Score; }
            return previousShotScore + shots.Sum();
        }
    }
}
